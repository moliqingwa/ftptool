from distutils.core import setup
import os

readme_fname = os.path.join(os.path.dirname(__file__), "README.rst")
readme_text = open(readme_fname).read()

setup(name="ftptool", version="0.1",
      url="https://moliqingwa@bitbucket.org/moliqingwa/ftptool.git",
      description="Higher-level interface to ftplib",
      author="Blogg Esse AB, Zhen Wang",
      author_email="wangzhen.1105@foxmail.com",
      long_description=readme_text,
      py_modules=["ftptool"])
