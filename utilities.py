#!/usr/bin/env python
# coding: utf-8

import os, sys
import logging
import configparser
import getpass

class ExceptionFtpDataOldVersion(Exception):
    pass

class ExceptionSectionNotFound(Exception):
    pass

def configure_logging(options):
    logger = logging.getLogger()
    if not options.quiet:
        logger.setLevel(logging.INFO)
    ch = logging.StreamHandler(sys.stderr)
    formatter = logging.Formatter("%(levelname)s: %(message)s")
    ch.setFormatter(formatter)
    logger.addHandler(ch)

class FtpData():
    password = None
    username = None
    hostname = None
    remotepath = None
    gitftpignore = None

def boolish(s):
    if s in ('1', 'true', 'y', 'ye', 'yes', 'on'):
        return True
    if s in ('0', 'false', 'n', 'no', 'off'):
        return False
    return None
    
def ask_ok(prompt, retries=4, complaint='Yes or no, please!'):
    while True:
        ok = input(prompt).lower()
        r = boolish(ok)
        if r is not None:
            return r
        retries = retries - 1
        if retries < 0:
            raise IOError('Wrong user input.')
        print (complaint)

def get_ftp_creds(ftpdata_path, options):
    """
    Retrieves the data to connect to the FTP from ftpdata_path (e.g. '.git/ftpdata')
    or interactively.

    ftpdata format example:

        [branch]
        username=me
        password=s00perP4zzw0rd
        hostname=ftp.hostname.com
        remotepath=/htdocs
        gitftpignore=.gitftpignore

    Please note that it isn't necessary to have this file,
    you'll be asked for the data every time you upload something.
    """
    ftpdata = ftpdata_path
    options.ftp = FtpData()
    cfg = configparser.ConfigParser()
    if os.path.isfile(ftpdata):
        logging.info("Using .git/ftpdata")
        cfg.read(ftpdata)

        if (not cfg.has_section(options.section)):
            if cfg.has_section('ftp'):
                raise ExceptionFtpDataOldVersion("Please rename the [ftp] section to [branch]. " +
                                        "Take a look at the README for more information")
            else:
                raise ExceptionSectionNotFound("Your .git/ftpdata file does not contain a section " +
                                     "named '%s'" % options.section)

        # just in case you do not want to store your ftp password.
        try:
            options.ftp.password = cfg.get(options.section, 'password')
        except configparser.NoOptionError:
            options.ftp.password = getpass.getpass('FTP Password: ')

        options.ftp.username = cfg.get(options.section, 'username')
        options.ftp.hostname = cfg.get(options.section, 'hostname')
        options.ftp.remotepath = cfg.get(options.section, 'remotepath')

        try:
            options.ftp.gitftpignore = cfg.get(options.section, 'gitftpignore')
        except configparser.NoOptionError:
            options.ftp.gitftpignore = '.gitftpignore'
    else:
        print ("Please configure settings of ftp information.")
        options.ftp.username = input('FTP Username: ')
        options.ftp.password = input('Password: ')
        options.ftp.hostname = input('FTP Hostname: ')
        options.ftp.remotepath = input('Remote Path: ')

        # set default branch
        if ask_ok("Should I write ftp details to .git/ftpdata? "):
            print("ok")
            cfg.add_section(options.section)
            cfg.set(options.section, 'username', options.ftp.username)
            cfg.set(options.section, 'password', options.ftp.password)
            cfg.set(options.section, 'hostname', options.ftp.hostname)
            cfg.set(options.section, 'remotepath', options.ftp.remotepath)
            f = open(ftpdata, 'w')
            cfg.write(f)

def parse_ftpignore(rawPatterns):
    patterns = []
    for pat in rawPatterns:
        pat = pat.rstrip()
        if not pat or pat.startswith('#'):
            continue
        patterns.append(pat)
    return patterns

def parse_file_property(line, files=[], subdirs=[], links=None):
    """Parse *line* and insert into either *files* or *subdirs* depending on
    whether the line is for a directory or not.

    This is used as the callback to the ftplib.FTPConnection.dir callback.
    """
    dst = None
    if line.startswith("d"):
        dst = subdirs
    elif line.startswith("-"):
        dst = files
    elif line.startswith("l"):
        dst = links
    else:
        raise ValueError("unknown line type %r" % line[:1])
    # No dst set for this type: ignore.
    if dst is None:
        return
    parts = line.split(None, 8)
    _, name = parts[0], parts[-1]
    dst.append(name)