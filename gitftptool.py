#!/usr/bin/env python
# coding: utf-8

import os
import textwrap
import optparse
import io
import re, fnmatch
import logging
import ftplib
import posixpath

from git import Blob, Repo, Submodule

from ftptool import FTPFileClient
from utilities import configure_logging, get_ftp_creds, parse_ftpignore

class ExceptionBranchNotFound(Exception):
    pass

def parse_args():
    usage = 'usage: gitftp [OPTIONS] [DIRECTORY]'
    desc = """\
           This script uploads a directory to a website via FTP,
           but is smart and only uploads file that have changed.
           """
    parser = optparse.OptionParser(usage, description=textwrap.dedent(desc))
    
    parser.add_option('-f', '--force', dest="force", action="store_true",   default=False, help="force the reupload of all files")
    parser.add_option('-q', '--quiet', dest="quiet", action="store_true", default=False, help="quiet output")
    parser.add_option('-r', '--revision', dest="revision", default=None, help="use this revision instead of the server stored one")
    parser.add_option('-b', '--branch', dest="branch", default=None, help="use this branch instead of the active one [Git-FTP only]")
    parser.add_option('-s', '--section', dest="section", default=None, help="use this section from ftpdata instead of branch name")
    
    options, args = parser.parse_args()
    
    configure_logging(options)
    
    if len(args) > 1:
        parser.error("too many arguments")
    if args:
        cwd = args[0]
    else:
        cwd = "."
    
    try:
        repo = Repo(cwd)
        ftpdata = os.path.join(repo.git_dir, "ftpdata")
        logging.info("Entering Git-FTP mode!")
    except:
        logging.info("Entering non-Git-FTP mode!")
        repo = None
        ftpdata = os.path.join(cwd, "ftpdata")
    
    if repo and not options.branch:
        options.branch = repo.active_branch.name
    else:
        options.branch = "master"
        
    if not options.section:
        options.section = options.branch
    
    get_ftp_creds(ftpdata, options)
    
    return repo, options, args

class GitFTPClient(FTPFileClient):
    
    def is_special_file(self, name):
        """Returns true if a file is some special Git metadata and not content."""
        return posixpath.basename(name) in ['.gitignore', '.gitattributes', '.gitmodules']
    
    def split_pattern(self, path):  # TODO: Improve skeevy code
        path = fnmatch.translate(path).split('\\/')
        for i, p in enumerate(path[:-1]):
            if p:
                path[i] = p + '\\Z(?ms)'
        return path

    def is_ignored(self, path, regex):
        regex = self.split_pattern(os.path.normcase(regex))
        path = os.path.normcase(path).split('/')
    
        regex_pos = path_pos = 0
        if regex[0] == '':  # leading slash - root dir must match
            if path[0] != '' or not re.match(regex[1], path[1]):
                return False
            regex_pos = path_pos = 2
    
        if not regex_pos:  # find beginning of regex
            for i, p in enumerate(path):
                if re.match(regex[0], p):
                    regex_pos = 1
                    path_pos = i + 1
                    break
            else:
                return False
    
        if len(path[path_pos:]) < len(regex[regex_pos:]):
            return False
    
        n = len(regex)
        for r in regex[regex_pos:]:  # match the rest
            if regex_pos + 1 == n:  # last item; if empty match anything
                if re.match(r, ''):
                    return True
    
            if not re.match(r, path[path_pos]):
                return False
            path_pos += 1
            regex_pos += 1
    
        return True

    def is_ignored_path(self, path, patterns, quiet=False):
        """Returns true if a filepath is ignored by gitftpignore."""
        if self.is_special_file(path):
            return True
        for pat in patterns:
            if self.is_ignored(path, pat):
                return True
        return False
    
    def upload_blob(self, blob, quiet=False):
        """
        Uploads a blob.  Pre-condition on ftp is that our current working
        directory is the root directory of the repository being uploaded
        (that means DON'T use ftp.cwd; we'll use full paths appropriately).
        """
        if not quiet:
            logging.info('Uploading ' + blob.path)
        try:
            self.delete(blob.path)
        except ftplib.error_perm:
            pass
        self.ftp_obj.storbinary('STOR ' + blob.path, blob.data_stream)
        try:
            pass#ftp.voidcmd('SITE CHMOD ' + format_mode(blob.mode) + ' ' + blob.path)
        except ftplib.error_perm:
            # Ignore Windows chmod errors
            logging.warning('Failed to chmod ' + blob.path)
            pass
    
    def get_empty_tree(self, repo):
        return repo.tree(repo.git.hash_object('-w', '-t', 'tree', os.devnull))

    def upload_diff(self, repo, oldtree, tree, base, ignored):
        """
        Upload  and/or delete items according to a Git diff between two trees.
    
        upload_diff requires, that the ftp working directory is set to the base
        of the current repository before it is called.
    
        Keyword arguments:
        repo    -- The git.Repo to upload objects from
        oldtree -- The old tree to diff against. An empty tree will cause a full
                   upload of the new tree.
        tree    -- The new tree. An empty tree will cause a full removal of all
                   objects of the old tree.
        ftp     -- The active ftplib.FTP object to upload contents to
        base    -- The list of base directory and submodule paths to upload contents
                   to in ftp.
                   For example, base = ['www', 'www']. base must exist and must not
                   have a trailing slash.
        ignored -- The list of patterns explicitly ignored by gitftpignore.
    
        """
        # -z is used so we don't have to deal with quotes in path matching
        diff = repo.git.diff("--name-status", "--no-renames", "-z", oldtree.hexsha, tree.hexsha)
        diff = iter(diff.split("\0"))
        for line in diff:
            if not line:
                continue
            status, file = line, next(diff)
            assert status in ['A', 'D', 'M']
    
            filepath = posixpath.join(*(['/'] + base[1:] + [file]))
            if self.is_ignored_path(filepath, ignored):
                logging.info('Skipped ' + filepath)
                continue
    
            if status == "D":
                try:
                    self.delete(file)
                    logging.info('Deleted ' + file)
                except ftplib.error_perm:
                    logging.warning('Failed to delete ' + file)
    
                # Now let's see if we need to remove some subdirectories
                def generate_parent_dirs(x):
                    # invariant: x is a filename
                    while '/' in x:
                        x = posixpath.dirname(x)
                        yield x
                for parent_dir in generate_parent_dirs(file):
                    try:
                        # unfortunately, parent_dir in tree doesn't work for subdirs
                        tree[parent_dir]
                    except KeyError:
                        try:
                            self.rmd(parent_dir)
                            logging.debug('Cleaned away ' + parent_dir)
                        except ftplib.error_perm:
                            logging.info('Did not clean away ' + parent_dir)
                            break
            else:
                node = tree[file]
    
                if status == "A":
                    # try building up the parent directory
                    subtree = tree
                    if isinstance(node, Blob):
                        directories = file.split("/")[:-1]
                    else:
                        # for submodules also add the directory itself
                        assert isinstance(node, Submodule)
                        directories = file.split("/")
                    for c in directories:
                        subtree = subtree / c
                        try:
                            self.mkd(subtree.path)
                        except ftplib.error_perm:
                            pass
    
                if isinstance(node, Blob):
                    self.upload_blob(node)
                else:
                    module = node.module()
                    module_tree = module.commit(node.hexsha).tree
                    if status == "A":
                        module_oldtree = self.get_empty_tree(module)
                    else:
                        oldnode = oldtree[file]
                        assert isinstance(oldnode, Submodule)  # TODO: What if not?
                        module_oldtree = module.commit(oldnode.hexsha).tree
                    module_base = base + [node.path]
                    logging.info('Entering submodule %s', node.path)
                    self.cwd(posixpath.join(*module_base))
                    self.upload_diff(module, module_oldtree, module_tree, module_base, ignored)
                    logging.info('Leaving submodule %s', node.path)
                    self.cwd(posixpath.join(*base))

def main(): 
    repo, options, _ = parse_args()
    
    base = options.ftp.remotepath
    logging.info("Base directory is %s", base)
    
    try:
        branch = next((h for h in repo.heads if h.name == options.branch))
    except StopIteration:
        raise ExceptionBranchNotFound
    commit = branch.commit
    tree = commit.tree
    
    # The standard FTP server control port
    FTP_PORT = 21
    gitftp = GitFTPClient.connect(host=options.ftp.hostname, port=FTP_PORT, user=options.ftp.username, password=options.ftp.password)
    gitftp.ftp_directory = base
    
    # Check revision
    hashValue = options.revision
    if not options.force and not hashValue:
        try:
            gitftp.get(posixpath.join(base, 'git-rev.txt'), 'git-rev.txt')
            with open('git-rev.txt', 'r') as f:
                hashFile = io.StringIO(f.read())
                hashValue = hashFile.getvalue().strip()
        except ftplib.error_perm:
            pass
    
    # Load ftpignore rules, if any
    patterns = []
    gitftpignore = os.path.join(repo.working_dir, options.ftp.gitftpignore)
    if os.path.isfile(gitftpignore):
        with open(gitftpignore, 'r') as ftpignore:
            patterns = parse_ftpignore(ftpignore)
        patterns.append('/' + options.ftp.gitftpignore)
    
    if not hashValue:
        # Diffing against an empty tree will cause a full upload.
        oldtree = gitftp.get_empty_tree(repo)
    else:
        oldtree = repo.commit(hashValue).tree

    if oldtree.hexsha == tree.hexsha:
        logging.info('Nothing to do!')
    else:
        gitftp.upload_diff(repo, oldtree, tree, [base], patterns)
    
    with open('git-rev.txt', 'wb') as f:
        f.write(commit.hexsha.encode())
    gitftp.put('git-rev.txt', posixpath.join(base, 'git-rev.txt'))
    
    gitftp.quit()

if __name__=="__main__":
    main()
    print ("Done")