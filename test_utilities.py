#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""Tests for utilities."""

import unittest

from utilities import boolish, parse_ftpignore, parse_file_property

class TestUtilities(unittest.TestCase):
    def test_boolish(self):
        self.assertEqual(boolish('1'), True)
        self.assertEqual(boolish('true'), True)
        self.assertEqual(boolish('y'), True)
        self.assertEqual(boolish('ye'), True)
        self.assertEqual(boolish('yes'), True)
        self.assertEqual(boolish('on'), True)
        
        self.assertEqual(boolish('0'), False)
        self.assertEqual(boolish('false'), False)
        self.assertEqual(boolish('n'), False)
        self.assertEqual(boolish('no'), False)
        self.assertEqual(boolish('off'), False)

    def test_parse_ftpignore(self):
        rawPatterns = [
            '#compiled python file',
            '*.pyc',
            '#temp file',
            '*.tmp']
        self.assertEqual(parse_ftpignore(rawPatterns),['*.pyc','*.tmp'])

    def test_parse_file_property(self):    
        files = []
        subdirs = []
        links = []
        
        line = 'drwxr-xr-x+ 1 zhen.wang Domain Users        0 四月 28 10:58 bin'
        parse_file_property(line, files, subdirs, links)
        self.assertEqual(subdirs, ['bin'])
        
        line = '-rwxr-xr-x  1 zhen.wang Administrators     59 四月  1 15:13 Cygwin.bat'
        parse_file_property(line, files, subdirs, links)
        self.assertEqual(files, ['Cygwin.bat'])
    
if __name__ == "__main__":
    unittest.main()
